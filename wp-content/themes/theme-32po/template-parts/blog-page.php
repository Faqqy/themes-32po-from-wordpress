<?php
/**
 * Template Name: Новости
 * */

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="news">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="news__container d-flex flex-row align-items-center justify-content-between">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/news-page.jpg" alt="">
					</div>
					<div class="news__title">
						<p>                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
						</p>
					</div>
					<div class="news__button">
						<button class="news__button-white">
							<a href="news__info-page.html">Подробнее</a>
						</button>
					</div>
				</div>

				<div class="news__container d-flex flex-row align-items-center justify-content-between">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/news-page.jpg" alt="">
					</div>
					<div class="news__title">
						<p>                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
						</p>
					</div>
					<div class="news__button">
						<button class="news__button-white">
							<a href="news__info-page.html">Подробнее</a>
						</button>
					</div>
				</div>

				<div class="news__container d-flex flex-row align-items-center justify-content-between">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/news-page.jpg" alt="">
					</div>
					<div class="news__title">
						<p>                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
						</p>
					</div>
					<div class="news__button">
						<button class="news__button-white">
							<a href="news__info-page.html">Подробнее</a>
						</button>
					</div>
				</div>

				<div class="news__container d-flex flex-row align-items-center justify-content-between">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/news-page.jpg" alt="">
					</div>
					<div class="news__title">
						<p>                            Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.
						</p>
					</div>
					<div class="news__button">
						<button class="news__button-white">
							<a href="news__info-page.html">Подробнее</a>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
