<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 32po
 */

if ( ! is_active_sidebar( 'sidebar-helpers' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-helpers' ); ?>
</aside><!-- #secondary -->

