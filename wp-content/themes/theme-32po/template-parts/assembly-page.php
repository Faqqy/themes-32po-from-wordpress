<?php
/**
 * Template Name: Производство
 * */

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="assembly">
	<div class="container">
		<div class="row">
			<div class="col-md-3 xs-hidden">
				<div class="column__right-sidebar">
					<div class="column__right-menu">
						<ul class="column__right__menu-list d-flex flex-column">
							<?php get_sidebar('info'); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-9 col-xs-12">
				<div class="assembly__text">
					<?php
					$post_id      = 29;
					$post_data    = get_post($post_id);
					$post_content = $post_data->post_content;
					?>
                    <p>
						<?php echo $post_content ?>
                    </p>
				</div>

				<div class="assembly__photo d-flex flex-row justify-content-between">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/assembly1.png" alt="">
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/assembly2.png" alt="">
					</div>
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/assembly3.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
