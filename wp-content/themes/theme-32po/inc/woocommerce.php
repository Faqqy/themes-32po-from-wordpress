<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package 32po
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)
 * @link https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 *
 * @return void
 */

//add_action( 'after_setup_theme', 'theme_32po_woocommerce_setup' );
//
//function theme_32po_woocommerce_scripts() {
//	wp_enqueue_style( 'theme-32po-woocommerce-style', get_template_directory_uri() . '/woocommerce.css', array(), _S_VERSION );
//
//	$font_path   = WC()->plugin_url() . '/assets/fonts/';
//	$inline_font = '@font-face {
//			font-family: "star";
//			src: url("' . $font_path . 'star.eot");
//			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
//				url("' . $font_path . 'star.woff") format("woff"),
//				url("' . $font_path . 'star.ttf") format("truetype"),
//				url("' . $font_path . 'star.svg#star") format("svg");
//			font-weight: normal;
//			font-style: normal;
//		}';
//
//	wp_add_inline_style( 'theme-32po-woocommerce-style', $inline_font );
//}
//add_action( 'wp_enqueue_scripts', 'theme_32po_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param array $classes CSS classes applied to the body tag.
 *
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function theme_32po_woocommerce_active_body_class( array $classes ): array {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'theme_32po_woocommerce_active_body_class' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function theme_32po_woocommerce_related_products_args( array $args ): array {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'theme_32po_woocommerce_related_products_args' );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'theme_32po_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function theme_32po_woocommerce_wrapper_before() {
		?>
			<main id="primary" class="site-main">
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'theme_32po_woocommerce_wrapper_before' );

if ( ! function_exists( 'theme_32po_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function theme_32po_woocommerce_wrapper_after() {
		?>
			</main><!-- #main -->
		<?php
	}
}
