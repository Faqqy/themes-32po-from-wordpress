<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action('woocommerce_before_main_content', 'po_add_only_arhive_sidebar', 50);
function po_add_only_arhive_sidebar() {
	if( ! is_product()) {
		woocommerce_get_sidebar();
	}
}

remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop', 'po_add_filter_sort_start', 10);
function po_add_filter_sort_start() {
	?>
    <div class="sort__product">
        <div class="sort__product-items d-flex flex-row justify-content-end">
			<?php woocommerce_catalog_ordering(); ?>
        </div>
    </div>
	<?php
}

//add_action( 'woocommerce_after_single_product_summary', 'po_single_category_slug' );
//
//function po_single_category_slug() {
//
//	if ( has_term( 'dilerskaya-produktsiya', 'product_cat' ) ) {
//		echo 'Something';
//	}
//
//}

//add_action( 'woocommerce_after_shop_loop_item', 'po_loop_per_product' );
//
//function po_loop_per_product() {
//
//	if ( has_term( 'dilerskaya-produktsiya', 'product_cat' ) ) {
//		echo 'Great chairs!';
//	}

add_action('woocommerce_before_main_content', 'po_add_archive_wrapper_start', 40);
function po_add_archive_wrapper_start() {
	?>
	    <div class="container">
	        <div class="row">

	<?php
}

add_action('woocommerce_after_main_content', 'po_add_archive_wrapper_end', 30);
function po_add_archive_wrapper_end() {
	?>
            </div>
        </div>
	<?php
}



add_action('woocommerce_before_main_content', 'po_archive_wrapper_content_start', 60);
function po_archive_wrapper_content_start() {

	if( ! is_product()) {
		?>
        <div class="col-md-9 col-xs-12">
		<?php
	}

}

add_action('woocommerce_after_main_content', 'po_archive_wrapper_content_end', 25);
function po_archive_wrapper_content_end() {
	?>
    </div>
	<?php
}

add_filter('post_class', 'po_add_class_loop_item');
function po_add_class_loop_item($classes) {
    if(is_shop() || is_product_taxonomy()){
	    $classes[] = 'product__list-item';
    }
    //get_pr($classes, false);
    return $classes;
}

remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
add_action('woocommerce_shop_loop_item_title', 'po_template_loop_product_title', 10);
function po_template_loop_product_title() {
    echo '<h3 class="product__name">' . get_the_title() . '</h3>';
}

add_filter('woocommerce_loop_add_to_cart_args', 'po_add_class_add_to_cart');
function po_add_class_add_to_cart($args){
    $args['class'] = $args['class'] . ' hidden__block-button' . ' wo__button-button__red';

    return $args;
}


