<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 32po
 */

if ( ! is_active_sidebar( 'sidebar-shop' ) ) {
	return;
}
?>



<header class="woocommerce-products-header">
    <section class="title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                        <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>





<aside id="secondary" class="widget-area col-md-3 d-flex flex-column filter">

	<?php dynamic_sidebar( 'sidebar-shop' ); ?>
</aside><!-- #secondary -->
