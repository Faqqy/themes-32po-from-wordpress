jQuery(function ($) {
    $('.search input[name="s"]').on('keyup', function () {
        let search = $('.search input[name="s"]').val();
        if (search.length < 4) {
            return false;
        }
        let data = {
            s:search,
            action: 'search-ajax',
            nonce: search.nonce
        };
        $.ajax({
            url: search.url,
            data: data,
            type: 'POST',
            datatype: 'json',
            beforeSend: function(xhr){

            },
            success: function(data){
                console.log(data);
            }
        });

    });
})