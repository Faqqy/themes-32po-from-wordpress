<?php
function po_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'po_add_woocommerce_support' );

add_action('after_setup_theme', 'crb_load');
function crb_load() {
	load_template(get_template_directory() . '/inc/carbon-fields/vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}

add_action('carbon_fields_register_fields', 'theme_32po_register_custom_fields');
function theme_32po_register_custom_fields() {
	require get_template_directory() . '/inc/custom-fields-options/metabox.php';
	require get_template_directory() . '/inc/custom-fields-options/theme-options.php';
}

/* Настройки темы */
require get_template_directory() . '/inc/theme-settings.php';
/* Подключение виджетов */
require get_template_directory() . '/inc/widget-areas.php';
/* Подключение стилей и скриптов */
require get_template_directory() . '/inc/enqueue-script-style.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';
/*
 * Вспомогательные функции
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Ajax
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Ajax
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Menu
 */
require get_template_directory() . '/inc/navigation.php';




/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-archive.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-remove.php';
	require get_template_directory() . '/woocommerce/includes/wc_functions_cart.php';

}
