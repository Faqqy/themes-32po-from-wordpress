<?php
/**
 * Template Name: Ведомости
 * */

$helperstext1 = carbon_get_the_post_meta('po_helpers_text1');
$helperstext2 = carbon_get_the_post_meta('po_helpers_text2');
$helperstext3 = carbon_get_the_post_meta('po_helpers_text3');
$helperstext4 = carbon_get_the_post_meta('po_helpers_text4');
$helperstextbottom = carbon_get_the_post_meta('po_helpers_text_bottom');

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="help">
	<div class="container">
		<div class="row">
			<div class="col-md-3 xs-hidden">
				<div class="column__right-sidebar">
					<div class="column__right-menu">
						<ul class="column__right__menu-list d-flex flex-column">
							<?php get_sidebar('helpers'); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-9 col-xs-12">
				<div class="help__text">
					<?php
					$post_id      = 519;
					$post_data    = get_post($post_id);
					$post_content = $post_data->post_content;
					?>
					<p>
						<?php echo $post_content ?>
					</p>
				</div>

				<div class="help__photo d-flex flex-row justify-content-between">
					<div class="help__photo-item">
						<?php
						$helpersfoto1 = carbon_get_the_post_meta('po_helpers_photo1');
						$photohelp= $helpersfoto1 ? wp_get_attachment_image_src($helpersfoto1, 'full') : '';
						?>
						<img src="<?php echo $photohelp[0];?>" alt="">
						<p><?php echo $helperstext1;?> </p>
					</div>
					<div class="help__photo-item">
						<?php
						$helpersfoto2 = carbon_get_the_post_meta('po_helpers_photo2');
						$photohelp= $helpersfoto2 ? wp_get_attachment_image_src($helpersfoto2, 'full') : '';
						?>
						<img src="<?php echo $photohelp[0];?>" alt="">
						<p><?php echo $helperstext2;?> </p>
					</div>
					<div class="help__photo-item">
						<?php
						$helpersfoto3 = carbon_get_the_post_meta('po_helpers_photo3');
						$photohelp= $helpersfoto3 ? wp_get_attachment_image_src($helpersfoto3, 'full') : '';
						?>
						<img src="<?php echo $photohelp[0];?>" alt="">
						<p><?php echo $helperstext3;?> </p>
					</div>
					<div class="help__photo-item">
						<?php
						$helpersfoto4 = carbon_get_the_post_meta('po_helpers_photo4');
						$photohelp= $helpersfoto4 ? wp_get_attachment_image_src($helpersfoto4, 'full') : '';
						?>
						<img src="<?php echo $photohelp[0];?>" alt="">
						<p><?php echo $helperstext4;?> </p>
					</div>
				</div>
			</div>
		</div>

		<div class="seo__text-bottom">
			<div class="row">
				<div class="col-md-12">
					<p>
						<?php echo $helperstextbottom; ?>
					</p>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>
