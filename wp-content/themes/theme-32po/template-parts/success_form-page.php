<?php
/**
 * Template Name: Страница спасибо! После отправки формы.
 * */
$editphone = carbon_get_theme_option('po_header_telephone');
$editphone2 = carbon_get_theme_option('po_header_telephone2');
$editemail = carbon_get_theme_option('po_header_mail');
$editadress = carbon_get_theme_option('po_header_adress');

$socialvk = carbon_get_theme_option('po_header_socialvk');
$socialinst = carbon_get_theme_option('po_header_socialinst');
$socialfb = carbon_get_theme_option('po_header_socialfb');
?>

<?php get_header(); ?>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="thanks">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="thanks__info">
					<p>
						Благодарим Вас за оставленное сообщение!
					</p>
					<p>
						В течении 15 минут наш менеджер с вами свяжется для уточнения деталей.
					</p>
				</div>

				<div class="contacts__phone contact__adress">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/black-phone.svg" alt="Наш телефон">
					<span><a href="tel:<?php echo $editphone;?>"><?php echo $editphone;?>,</a></span>
					<span><a href="tel:<?php echo $editphone2;?>"><?php echo $editphone2;?></a></span>
					<br>
<!--					<img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/mail.png" alt="Наша почта" style="width: 20px;">-->
<!--					<span><a href="mailto:--><?php //echo $editemail;?><!--">--><?php //echo $editemail;?><!--</a></span>-->
<!--					<br>-->
					<img src="<?php echo get_template_directory_uri();?>/assets/images/black-map.svg" alt="Наш адрес">
					<span><?php echo $editadress;?></span>
				</div>

				<div class="social__icons">
					<a class="social__icons-item" href="<?php echo $socialvk;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-vk.svg" alt="Вконтакте"></a>
					<a class="social__icons-item" href="<?php echo $socialinst;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-inst.svg" alt="Инстаграмм"></a>
					<a class="social__icons-item" href="<?php echo $socialfb;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-fb.svg" alt="Фейсбук"></a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>