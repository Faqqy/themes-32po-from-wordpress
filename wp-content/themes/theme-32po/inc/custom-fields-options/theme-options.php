<?php
if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.0' );
}


use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', 'Настройки темы' )
	->set_icon( 'dashicons-carrot' )
	->add_tab('Шапка', array(
		Field::make( 'image', 'po_header_logo', __( 'Логотип' ) ),
		Field::make( 'text', 'po_header_telephone', __( 'Изменить первый телефон' ) )
			->set_default_value('8 (4832) 58-80-47')
				->set_width(30),
		Field::make( 'text', 'po_header_telephone2', __( 'Изменить второй телефон' ) )
		     ->set_default_value('8 (4832) 32-10-67')
		     ->set_width(30),
		Field::make( 'text', 'po_header_mail', __( 'Изменить E-mail' ) )
		     ->set_default_value('promob32@yandex.ru')
		     ->set_width(30),
		Field::make( 'text', 'po_header_adress', __( 'Изменить адрес' ) )
		     ->set_default_value('Брянск, ул. Литейная 112'),
		Field::make( 'text', 'po_header_socialvk', __( 'Ссылка "Вконтакте"' ) )
		     ->set_default_value('#')
		        ->set_width(30),
		Field::make( 'text', 'po_header_socialinst', __( 'Ссылка "Инстаграмм"' ) )
		     ->set_default_value('#')
		        ->set_width(30),
		Field::make( 'text', 'po_header_socialfb', __( 'Ссылка "Фейсбук"' ) )
		     ->set_default_value('#')
		        ->set_width(30)
	) )
	->add_tab('Подвал', array(
		Field::make( 'image', 'po_footer_logo', __( 'Логотип' ) ),
		Field::make( 'text', 'po_footer_copyright', __( 'Изменить копирайт' ) )
			->set_default_value('© 2021. Промоборудование. Все права защищены'),

	) )
	->add_tab('Главная страница', array(
		Field::make( 'image', 'po_photo_banner1', __( 'Первая картинка баннера' ) )
			->set_width(50),
		Field::make( 'text', 'po_photo_text_banner1', __( 'Текст к первой картинке баннера' ) )
			->set_width(50),
		Field::make( 'image', 'po_photo_banner2', __( 'Вторая картинка баннера' ) )
			->set_width(50),
		Field::make( 'text', 'po_photo_text_banner2', __( 'Текст к второй картинке баннера' ) )
			->set_width(50),
		Field::make( 'image', 'po_photo_banner3', __( 'Третья картинка баннера' ) )
			->set_width(50),
		Field::make( 'text', 'po_photo_text_banner3', __( 'Текст к третей картинке баннера' ) )
			->set_width(50),
		Field::make( 'text', 'po_text_prom', __( 'Текст к заголовку "Промоборудование"' ) )


	) );


Container::make('post_meta', 'Фотографии внизу текста')
		 ->where( 'post_type', '=', 'page' )
		 ->where( 'post_template', '=', 'template-parts/manufacturing-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
		         ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
		         ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
		         ->set_width(30)
         ] );


Container::make('post_meta', 'Фотографии внизу текста')
		 ->where( 'post_type', '=', 'page' )
		 ->where( 'post_template', '=', 'template-parts/laser_cut-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/termal-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/mehanical-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/Repair_and_service-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/Manufacturing_to_order-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/repair-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/design services.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Фотографии внизу текста')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/lasercut_manuf-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_manufacturing_photo1', __( 'Первая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo2', __( 'Вторая фотография' ) )
	              ->set_width(30),
	         Field::make( 'image', 'po_manufacturing_photo3', __( 'Третья фотография' ) )
	              ->set_width(30)
         ] );

Container::make('post_meta', 'Ссылка на скачивание файла')
	->where( 'post_type', '=', 'page' )
	->where( 'post_template', '=', 'template-parts/rubric-page.php' )
	->add_fields( [
		Field::make( 'text', 'po_text_rubrik1', __( 'Текст ссылки' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_link_rubrik1', __( 'Вставьте ссылку' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_text_rubrik2', __( 'Текст ссылки' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_link_rubrik2', __( 'Вставьте ссылку' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_text_rubrik3', __( 'Текст ссылки' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_link_rubrik3', __( 'Вставьте ссылку' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_text_rubrik4', __( 'Текст ссылки' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_link_rubrik4', __( 'Вставьте ссылку' ) )
		     ->set_width(50),
	] );

Container::make('post_meta', 'Ссылка на скачивание файла')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/props-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_text_rubrik1', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik1', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik2', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik2', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik3', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik3', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik4', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik4', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
         ] );

Container::make('post_meta', 'Ссылка на скачивание файла')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/reportable-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_text_rubrik1', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik1', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik2', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik2', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik3', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik3', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik4', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik4', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
         ] );

Container::make('post_meta', 'Ссылка на скачивание файла')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/affilate-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_text_rubrik1', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik1', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik2', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik2', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik3', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik3', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik4', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik4', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
         ] );

Container::make('post_meta', 'Ссылка на скачивание файла')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/information-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_text_rubrik1', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik1', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik2', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik2', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik3', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik3', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_text_rubrik4', __( 'Текст ссылки' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_link_rubrik4', __( 'Вставьте ссылку' ) )
	              ->set_width(50),
         ] );

Container::make('post_meta', 'Фотографии и текст помощь')
	->where( 'post_type', '=', 'page' )
	->where( 'post_template', '=', 'template-parts/help-page.php' )
	->add_fields( [
		Field::make( 'image', 'po_helpers_photo1', __( 'Первая фотография' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_helpers_text1', __( 'Текст к первой фотографии' ) )
		     ->set_width(50),
		Field::make( 'image', 'po_helpers_photo2', __( 'Вторая фотография' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_helpers_text2', __( 'Текст ко второй фотографии' ) )
		     ->set_width(50),
		Field::make( 'image', 'po_helpers_photo3', __( 'Третья фотография' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_helpers_text3', __( 'Текст к третей фотографии' ) )
		     ->set_width(50),
		Field::make( 'image', 'po_helpers_photo4', __( 'Четвертая фотография' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_helpers_text4', __( 'Текст к четвертой фотографии' ) )
		     ->set_width(50),
		Field::make( 'text', 'po_helpers_text_bottom', __( 'Текст снизу страницы' ) )
	] );

Container::make('post_meta', 'Фотографии и текст Руководств')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/direction-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_helpers_photo1', __( 'Первая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text1', __( 'Текст к первой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo2', __( 'Вторая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text2', __( 'Текст ко второй фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo3', __( 'Третья фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text3', __( 'Текст к третей фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo4', __( 'Четвертая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text4', __( 'Текст к четвертой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text_bottom', __( 'Текст снизу страницы' ) )
         ] );

Container::make('post_meta', 'Фотографии и текст Правил')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/rules-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_helpers_photo1', __( 'Первая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text1', __( 'Текст к первой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo2', __( 'Вторая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text2', __( 'Текст ко второй фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo3', __( 'Третья фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text3', __( 'Текст к третей фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo4', __( 'Четвертая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text4', __( 'Текст к четвертой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text_bottom', __( 'Текст снизу страницы' ) )
         ] );

Container::make('post_meta', 'Фотографии и текст Инструкций')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/instruction-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_helpers_photo1', __( 'Первая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text1', __( 'Текст к первой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo2', __( 'Вторая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text2', __( 'Текст ко второй фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo3', __( 'Третья фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text3', __( 'Текст к третей фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo4', __( 'Четвертая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text4', __( 'Текст к четвертой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text_bottom', __( 'Текст снизу страницы' ) )
         ] );

Container::make('post_meta', 'Фотографии и текст Ведомостей')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/payroll-page.php' )
         ->add_fields( [
	         Field::make( 'image', 'po_helpers_photo1', __( 'Первая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text1', __( 'Текст к первой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo2', __( 'Вторая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text2', __( 'Текст ко второй фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo3', __( 'Третья фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text3', __( 'Текст к третей фотографии' ) )
	              ->set_width(50),
	         Field::make( 'image', 'po_helpers_photo4', __( 'Четвертая фотография' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text4', __( 'Текст к четвертой фотографии' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_helpers_text_bottom', __( 'Текст снизу страницы' ) )
         ] );


Container::make('post_meta', 'Редактирование текста доставки')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/dilevery-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_dilevery_text_top', __( 'Текст сверху страницы' ) ),
	         Field::make( 'text', 'po_dilevery_text_bottom', __( 'Текст снизу страницы' ) )
         ] );

Container::make('post_meta', 'Ссылка на карту в контактах')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/contacts-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_map_link', __( 'Ссылка на карту яндекса' ) )
         ] );


Container::make('post_meta', 'Ссылки на страницы и их названия')
         ->where( 'post_type', '=', 'page' )
         ->where( 'post_template', '=', 'template-parts/production-page.php' )
         ->add_fields( [
	         Field::make( 'text', 'po_product_text1', __( 'Название категории' ) )
		         ->set_width(50)
		         ->set_default_value('Дилерская продукция'),
	         Field::make( 'text', 'po_product_link1', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text2', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('Основная продукция'),
	         Field::make( 'text', 'po_product_link2', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text3', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('Остальная продукция'),
	         Field::make( 'text', 'po_product_link3', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text4', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('Спецпредложения'),
	         Field::make( 'text', 'po_product_link4', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text5', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('Наша продукция'),
	         Field::make( 'text', 'po_product_link5', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text6', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('На складе'),
	         Field::make( 'text', 'po_product_link6', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
	         Field::make( 'text', 'po_product_text7', __( 'Название категории' ) )
	              ->set_width(50)
	              ->set_default_value('Неликвиды'),
	         Field::make( 'text', 'po_product_link7', __( 'Ссылка на категорию' ) )
	              ->set_width(50),
         ] );

