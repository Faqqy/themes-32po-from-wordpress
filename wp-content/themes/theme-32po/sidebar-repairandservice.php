<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 32po
 */

if ( ! is_active_sidebar( 'sidebar-repairandservice' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-repairandservice' ); ?>
</aside><!-- #secondary -->