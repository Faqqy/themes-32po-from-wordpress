<?php
/**
 * Template Name: Контакты
 * */
$editphone = carbon_get_theme_option('po_header_telephone');
$editphone2 = carbon_get_theme_option('po_header_telephone2');
$editemail = carbon_get_theme_option('po_header_mail');

$editadress = carbon_get_theme_option('po_header_adress');

$socialvk = carbon_get_theme_option('po_header_socialvk');
$socialinst = carbon_get_theme_option('po_header_socialinst');
$socialfb = carbon_get_theme_option('po_header_socialfb');

$editmaplink = carbon_get_the_post_meta('po_map_link');

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="contacts">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-wrap flex-row align-items-center justify-content-between">
				<div class="contacts__map">
                    <iframe src="<?php echo $editmaplink; ?>" width="100%" height="400" frameborder="0"></iframe>
				</div>
				<div class="contacts__info">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/black-phone.svg" alt="Наш телефон">
                    <span><a href="tel:<?php echo $editphone;?>"><?php echo $editphone;?>,</a></span>

                    <span><a href="tel:<?php echo $editphone2;?>"><?php echo $editphone2;?></a></span>
                    <br>
					<img src="<?php echo get_template_directory_uri();?>/assets/images/black-map.svg" alt="Наш адрес">
                    <span><?php echo $editadress;?></span>

					<div class="social__icons">
						<a class="social__icons-item" href="<?php echo $socialvk;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-vk.svg" alt="Вконтакте"></a>
						<a class="social__icons-item" href="<?php echo $socialinst;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-inst.svg" alt="Инстаграмм"></a>
						<a class="social__icons-item" href="<?php echo $socialfb;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/black-fb.svg" alt="Фейсбук"></a>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form__contact">
				<div class="col-md-6">
<!--					<form action="#" name="form__fb-contacts" class="form d-flex flex-column justify-content-center">-->
<!--						<input type="text" name="name" placeholder="Ваше имя...">-->
<!--						<input type="text" name="company" placeholder="Компания...">-->
<!--						<input type="number" name="phone" placeholder="Ваш телефон...">-->
<!--						<input type="email" name="email" placeholder="Ваша почта...">-->
<!--						<div class="politics">-->
<!--							<p>Нажимая кнопку Отправить вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>-->
<!--						</div>-->
<!--						<div class="button__send-form">-->
<!--							<button class="button button__red">-->
<!--								<span>Отправить</span> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/Arrow.svg" alt="Подробнее">-->
<!--							</button>-->
<!--						</div>-->
<!--					</form>-->
                    <?php echo do_shortcode('[contact-form-7 id="649" title="Форма обратной связи для страницы контактов"]');?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
