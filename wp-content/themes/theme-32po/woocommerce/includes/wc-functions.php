<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
add_action('woocommerce_before_main_content', 'po__add__breadcrumbs', 20);
function po__add__breadcrumbs() {
	?>
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 d-flex flex-row">
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
        </div>
    </section>
<?php
}

add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 10);

add_action('woocommerce_before_single_product_summary', 'po_wrapper_product_start', 5);
function po_wrapper_product_start() {
    ?>
        <div class="container">
            <div class="row">
    <?php
}

add_action('woocommerce_after_single_product_summary', 'po_wrapper_product_end', 5);
function po_wrapper_product_end() {
	?>
            </div>
        </div>
	<?php
}

add_action('woocommerce_before_single_product_summary', 'po_wrapper_product_entry_start', 35);
function po_wrapper_product_entry_start() {
	?>
    <div class="col-md-6 col-xs-12 product__right">
	<?php
}

add_action('woocommerce_after_single_product_summary', 'po_wrapper_product_entry_end', 5);
function po_wrapper_product_entry_end() {
	?>
    </div>
	<?php
}

add_action('woocommerce_before_single_product_summary', 'po_wrapper_product_image_start', 5);
function po_wrapper_product_image_start() {
	?>
    <div class="col-md-6 col-xs-12">
	<?php
}

add_action('woocommerce_after_single_product_summary', 'po_wrapper_product_image_end', 25);
function po_wrapper_product_image_end() {
	?>
    </div>
	<?php
}






add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 5);

add_filter('woocommerce_product_tabs', 'po_product_tabs');
function po_product_tabs($product_tabs) {
    if ( ! empty( $product_tabs ) ) : ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs">
                    <div id="horizontalTab1" class="woocommerce-tabs wc-tabs-wrapper additional_info tabs__content">
                        <ul class="tabs wc-tabs tabs__nav-list d-flex flex-row justify-content-start" role="tablist">
                            <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                                <li class="<?php echo esc_attr( $key ); ?>_tab resp-tab-item" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
                                    <a onclick="return false;" href="#tab-<?php echo esc_attr( $key ); ?>">
                                        <?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
                                    </a href="#tab-<?php echo esc_attr( $key ); ?>">
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                            <div class="resp-tab-content resp-tab-content-active additional_info_grid" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
                                <?php
                                if ( isset( $product_tab['callback'] ) ) {
                                    call_user_func( $product_tab['callback'], $key, $product_tab );
                                }
                                ?>
                            </div>
                        <?php endforeach; ?>

                        <?php do_action( 'woocommerce_product_after_tabs' ); ?>
                            <script type="text/javascript">
                                jQuery(document).ready(function () {
                                    jQuery('#horizontalTab1').easyResponsiveTabs({
                                        type: 'default', //Types: default, vertical, accordion
                                        width: 'auto', //auto or any width like 600px
                                        fit: true   // 100% fit in a container
                                    });
                                });
                            </script>

                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                if(jQuery('#tab-title-description')).hasClass();
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif;
}

add_filter('woocommerce_breadcrumb_defaults','in_woocommerce_breadcrumb_defaults');

function in_woocommerce_breadcrumb_defaults($args){
	$args['delimiter']   = ' > ';
	return $args;
}

add_filter( 'woocommerce_product_additional_information_heading', 'po_heading_tab_desc_remove' );
add_filter( 'woocommerce_product_description_heading', 'po_heading_tab_desc_remove' );
function po_heading_tab_desc_remove($header){
	$header = false;
	return $header;
}





