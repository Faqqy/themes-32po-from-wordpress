<?php

//if ( ! defined('ABSPATH') ) {
//	exit;
//}

if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.0' );
}

add_action( 'wp_enqueue_scripts', 'theme_32po_style' );
function theme_32po_style() {
	wp_enqueue_style( 'theme-32po-style', get_stylesheet_uri(), array('theme-32po-style-bootstrap-reboot', 'theme-32po-style-bootstrap-grid'), null );
	wp_enqueue_style( 'theme-32po-style-bootstrap-reboot', get_template_directory_uri() . '/assets/libs/bootstrap/css/bootstrap-reboot.min.css', array(), null );
	wp_enqueue_style( 'theme-32po-style-bootstrap-grid', get_template_directory_uri() . '/assets/libs/bootstrap/css/bootstrap-grid.min.css', array(), null );
}

add_action( 'wp_enqueue_scripts', 'theme_32po_scripts' );
function theme_32po_scripts() {

	wp_enqueue_script( 'theme-32po-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), null, true );


//	wp_enqueue_script( 'theme-32po-script-jquery', get_template_directory_uri() . '/assets/libs/jquery/jquery-3.6.0.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'theme-32po-script-slider', get_template_directory_uri() . '/assets/js/slider-home.js', array(), null, false );
	wp_enqueue_script( 'theme-32po-script-tabs', get_template_directory_uri() . '/assets/js/tabs.js', array(), null, false );
	wp_enqueue_script( 'theme-32po-wc-script-zoom', get_template_directory_uri() . '/assets/js/zoom/jquery.zoom.min.js', array(), null, false );
	wp_enqueue_script( 'theme-32po-wc-script-zoom', get_template_directory_uri() . '/assets/js/flexslider/jquery.flexslider.min.js', array(), null, false );
	wp_enqueue_script( 'theme-32po-search', get_template_directory_uri() . '/assets/js/ajax-search.js', array(jquery), null, true );
	wp_localize_script('ajax-search', 'search' , array(
		'url' => admin_url('admin-ajax.php'),
		'nonce' => wp_create_nonce('search-nonce')
	));

	wp_enqueue_script( 'theme-32po-script-common', get_template_directory_uri() . '/assets/js/common.js', array(jquery), null,true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_dequeue_style( 'wcqi-css' );
}

