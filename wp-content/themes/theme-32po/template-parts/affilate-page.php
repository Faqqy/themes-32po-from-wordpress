<?php
/**
 * Template Name: Аффилированные лица
 * */


$rubriktext1 = carbon_get_the_post_meta('po_text_rubrik1');
$rubriklink1 = carbon_get_the_post_meta('po_link_rubrik1');

$rubriktext2 = carbon_get_the_post_meta('po_text_rubrik2');
$rubriklink2 = carbon_get_the_post_meta('po_link_rubrik2');

$rubriktext3 = carbon_get_the_post_meta('po_text_rubrik3');
$rubriklink3 = carbon_get_the_post_meta('po_link_rubrik3');

$rubriktext4 = carbon_get_the_post_meta('po_text_rubrik4');
$rubriklink4 = carbon_get_the_post_meta('po_link_rubrik4');



?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="repair">
	<div class="container">
		<div class="row">
			<div class="col-md-3 xs-hidden">
				<div class="column__right-sidebar">
					<div class="column__right-menu">
						<ul class="column__right__menu-list d-flex flex-column">
							<?php get_sidebar('rubrik'); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-9 col-xs-12">
				<div class="assembly__text">
					<?php
					$post_id      = 505;
					$post_data    = get_post($post_id);
					$post_content = $post_data->post_content;
					?>
					<p>
						<?php echo $post_content ?>
					</p>
				</div>

				<div class="info__text-list">
					<ul>
						<li><img src="<?php echo get_template_directory_uri();?>/assets/images/gear-product-list.svg" alt=""><a download href="<?php echo $rubriklink1;?>"><?php echo $rubriktext1;?></a></li>
						<li><img src="<?php echo get_template_directory_uri();?>/assets/images/gear-product-list.svg" alt=""><a download href="<?php echo $rubriklink2;?>"><?php echo $rubriktext2;?></a></li>
						<li><img src="<?php echo get_template_directory_uri();?>/assets/images/gear-product-list.svg" alt=""><a download href="<?php echo $rubriklink3;?>"><?php echo $rubriktext3;?></a></li>
						<li><img src="<?php echo get_template_directory_uri();?>/assets/images/gear-product-list.svg" alt=""><a download href="<?php echo $rubriklink4;?>"><?php echo $rubriktext4;?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>
