<?php
/**
 * Template Name: Продукция
 * */


$editproducttext1 = carbon_get_the_post_meta('po_product_text1');
$editproductlink1 = carbon_get_the_post_meta('po_product_link1');

$editproducttext2 = carbon_get_the_post_meta('po_product_text2');
$editproductlink2 = carbon_get_the_post_meta('po_product_link2');

$editproducttext3 = carbon_get_the_post_meta('po_product_text3');
$editproductlink3 = carbon_get_the_post_meta('po_product_link3');

$editproducttext4 = carbon_get_the_post_meta('po_product_text4');
$editproductlink4 = carbon_get_the_post_meta('po_product_link4');

$editproducttext5 = carbon_get_the_post_meta('po_product_text5');
$editproductlink5 = carbon_get_the_post_meta('po_product_link5');

$editproducttext6 = carbon_get_the_post_meta('po_product_text6');
$editproductlink6 = carbon_get_the_post_meta('po_product_link6');

$editproducttext7 = carbon_get_the_post_meta('po_product_text7');
$editproductlink7 = carbon_get_the_post_meta('po_product_link7');


?>


<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="absolution__container">
	<div class="absolution__container-text">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
					$post_id      = 27;
					$post_data    = get_post($post_id);
					$post_content = $post_data->post_content;
					?>
                    <p>
						<?php echo $post_content ?>
                    </p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="items__product-fly">
						<div class="absolution__container-item-1">
							<a href="<?php echo $editproductlink1;?>"><?php echo $editproducttext1;?></a>
						</div>
						<img class="one__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<div class="absolution__container-item-2">
							<a href="<?php echo $editproductlink2;?>"><?php echo $editproducttext2;?></a>
						</div>
						<img class="three__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<img class="four__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<img class="five__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<img class="six__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<img class="two__img" src="<?php echo get_template_directory_uri();?>/assets/images/r2.png" alt="">
						<div class="absolution__container-item-3">
							<a href="<?php echo $editproductlink3;?>"><?php echo $editproducttext3;?></a>
						</div>
					</div>
					<div class="items__product-fly2">
						<div class="absolution__container-item-4">
                            <a href="<?php echo $editproductlink4;?>"><?php echo $editproducttext4;?></a>
						</div>
						<div class="absolution__container-item-5">
                            <a href="<?php echo $editproductlink5;?>"><?php echo $editproducttext5;?></a>
						</div>
						<div class="absolution__container-item-6">
                            <a href="<?php echo $editproductlink6;?>"><?php echo $editproducttext6;?></a>
						</div>
					</div>
					<div class="items__product-fly2">
						<div class="absolution__container-item-7">
                            <a href="<?php echo $editproductlink7;?>"><?php echo $editproducttext7;?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>
