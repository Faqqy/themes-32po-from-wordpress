<?php
/**
 * Template Name: Ремонт и обслуживание
 * */

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="repair">
	<div class="container">
		<div class="row">
			<div class="col-md-3 xs-hidden">
				<div class="column__right-sidebar">
					<div class="column__right-menu">
						<ul class="column__right__menu-list d-flex flex-column">
							<?php get_sidebar('repairandservice'); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-9 col-xs-12">
				<div class="assembly__text">
					<?php
					$post_id      = 487;
					$post_data    = get_post($post_id);
					$post_content = $post_data->post_content;
					?>
					<p>
						<?php echo $post_content ?>
					</p>
				</div>

				<div class="assembly__photo d-flex flex-row justify-content-between">
					<div class="image">

						<?php
						$editphoto1 = carbon_get_the_post_meta('po_manufacturing_photo1');
						$photo = $editphoto1 ? wp_get_attachment_image_src($editphoto1, 'full') : '';
						?>
						<img src="<?php echo $photo[0];?>" alt="">
					</div>
					<div class="image">
						<?php
						$editphoto2 = carbon_get_the_post_meta('po_manufacturing_photo2');
						$photo = $editphoto2 ? wp_get_attachment_image_src($editphoto2, 'full') : '';
						?>
						<img src="<?php echo $photo[0];?>" alt="">
					</div>
					<div class="image">
						<?php
						$editphoto3 = carbon_get_the_post_meta('po_manufacturing_photo3');
						$photo = $editphoto3 ? wp_get_attachment_image_src($editphoto3, 'full') : '';
						?>
						<img src="<?php echo $photo[0];?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>

