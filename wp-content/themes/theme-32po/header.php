<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 32po
 */

?>
<?php
$editphone = carbon_get_theme_option('po_header_telephone');
$editphone2 = carbon_get_theme_option('po_header_telephone2');
$editemail = carbon_get_theme_option('po_header_mail');

$editadress = carbon_get_theme_option('po_header_adress');

$socialvk = carbon_get_theme_option('po_header_socialvk');
$socialinst = carbon_get_theme_option('po_header_socialinst');
$socialfb = carbon_get_theme_option('po_header_socialfb');
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'theme-32po' ); ?></a>

        <header id="masthead" class="header">
            <div class="header__top">
                <div class="mobile__info contacts__phone contact__adress d-flex justify-content-between">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/phone-icon.svg" alt="Наш телефон">
                    <span><a href="tel:<?php echo $editphone;?>"><?php echo $editphone;?></a></span> <br>
                    <span><a href="tel:<?php echo $editphone2;?>"><?php echo $editphone2;?></a></span> <br>

                    <img src="<?php echo get_template_directory_uri();?>/assets/images/adress-icon.svg" alt="Наш адрес">
                    <span><?php echo $editadress;?></span>
                </div>
                <div class="container__mobile d-flex align-items-center justify-content-between">
                    <div class="logo">
                        <?php $logo_id = carbon_get_theme_option('po_header_logo');
                        $logo = $logo_id ? wp_get_attachment_image_src($logo_id, 'full') : '';
                        ?>
                        <img class="logo__header" src="<?php echo $logo[0];?>" alt="Промоборудование">
                    </div>
                    <div class="social">
                        <div class="social__icons">
                            <a class="social__icons-item" href="<?php echo $socialvk;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/vk-icons.svg" alt="Вконтакте"></a>
                            <a class="social__icons-item" href="<?php echo $socialinst;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/inst-icons.svg" alt="Инстаграмм"></a>
                            <a class="social__icons-item" href="<?php echo $socialfb;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/fb-icons.svg" alt="Фейсбук"></a>
                        </div>
                    </div>
                    <div class="mobile__menu">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/burger-menu.png" alt="">
                    </div>
                </div>
                <div class="container container__desktop">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <div class="logo">
	                            <?php $logo_id = carbon_get_theme_option('po_header_logo');
	                            $logo = $logo_id ? wp_get_attachment_image_src($logo_id, 'full') : '';
	                            ?>
                                <a href="<?php echo home_url('/');?>"><img class="logo__header" src="<?php echo $logo[0];?>" alt="Промоборудование"></a>
                            </div>
                        </div>

                        <div class="col-md-3 cool-sm-3 col-xs-12 d-flex justify-content-between">
                            <div class="contacts d-flex align-items-center">
                                <div class="contacts__phone contact__adress" style="font-size: 16px;">
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/phone-icon.svg" alt="Наш телефон">
                                    <span><a href="tel:<?php echo $editphone;?>"><?php echo $editphone;?>,</a></span>
                                    <span><a href="tel:<?php echo $editphone2;?>"><?php echo $editphone2;?></a></span>
                                    <br>
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/mail.png" alt="Наша почта" style="width: 20px;">
                                    <span><a href="mailto:<?php echo $editemail;?>"><?php echo $editemail;?></a></span>
                                    <br>
                                    <img src="<?php echo get_template_directory_uri();?>/assets/images/adress-icon.svg" alt="Наш адрес">
                                    <span><?php echo $editadress;?></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 d-sm-none d-md-flex align-items-center justify-content-between">
                            <div class="social">
                                <div class="social__icons">
                                    <a class="social__icons-item" href="<?php echo $socialvk;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/vk-icons.svg" alt="Вконтакте"></a>
                                    <a class="social__icons-item" href="<?php echo $socialinst;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/inst-icons.svg" alt="Инстаграмм"></a>
                                    <a class="social__icons-item" href="<?php echo $socialfb;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/fb-icons.svg" alt="Фейсбук"></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 d-sm-none d-md-flex align-items-center justify-content-center">
                            <div class="input-group search search__form">
                                <form method="post" action="<?php esc_url(home_url('/'))?>">
                                    <input type="text" value="<?php get_search_query();?>" name="s" class="form-control search__form-input" placeholder="Поиск по сайту ..." aria-label="Поиск по сайту ...">
                                    <button type="submit" class="btn search__form-button" value="Поиск"><img src="<?php echo get_template_directory_uri();?>/assets/images/search-icons.svg" alt="Найти"></button>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-1 col-xs-12 d-flex align-items-center justify-content-center">
                            <div class="logging">
                                <button class="btn logging__button" type="button"><img src="<?php echo get_template_directory_uri();?>/assets/images/lc-icons.svg" alt="Личный кабинет" class="logging__icon"></button>
                            </div>
                        </div>

                        <div class="col-md-1 col-xs-12 d-flex align-items-center justify-content-center">
                            <?php theme_32po_woocommerce_cart_link(); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="header__bottom">
                <div class="container">
                 <?php po_primary_menu();?>
                </div>
            </div>
        </header>

    <script>
        jQuery(document).ready(function() {
            jQuery('input,textarea').focus(function(){
                jQuery(this).data('placeholder',jQuery(this).attr('placeholder'))
                    .attr('placeholder','');
            }).blur(function(){
                jQuery(this).attr('placeholder',jQuery(this).data('placeholder'));
            })

            jQuery('.open-menu').hover(
                function() {
                    jQuery('.sub__menu.sub').css('display', 'block');
                }, function() {
                    jQuery('.sub__menu.sub').css('display', 'none');
                }
            );

            jQuery( ".sub__menu.sub.fade" ).hover(function() {
                jQuery( this ).fadeOut( 100 );
                jQuery( this ).fadeIn( 500 );
            });

            jQuery('.mobile__menu').click(function() {
                jQuery('.menu__mobile-container').addClass('open-mobile');
                jQuery('body').addClass('open-modal');
            });

            jQuery('.close__button-menu').click(function(){
                jQuery('.menu__mobile-container').removeClass('open-mobile');
                jQuery('body').removeClass('open-modal');
            });

            jQuery('.open-modal').click(function(){
                jQuery('.menu__mobile-container').removeClass('open-mobile');
                jQuery('body').removeClass('open-modal');
            });

            jQuery(document).mouseup(function (e) {
                let container = jQuery('.open-mobile');
                if (container.has(e.target).length === 0){
                    jQuery('.menu__mobile-container').removeClass('open-mobile');
                    jQuery('body').removeClass('open-modal');
                }
            });
        });

    </script>

    <div class="wrapper">

<!--		<nav id="site-navigation" class="main-navigation">-->
<!--			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //esc_html_e( 'Primary Menu', 'theme-32po' ); ?><!--</button>-->
<!--			--><?php
//			wp_nav_menu(
//				array(
//					'theme_location' => 'menu-1',
//					'menu_id'        => 'primary-menu',
//				)
//			);
//			?>
<!--		</nav>-->
