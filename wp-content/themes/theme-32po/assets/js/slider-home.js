document.addEventListener('DOMContentLoaded', function() {

    const slides = document.querySelectorAll('.offer__slide'),
        prev = document.querySelector('.prev'),
        next = document.querySelector('.next'),
        slidesWrapper = document.querySelector('.top__slider-wrapper'),
        slidesField = document.querySelector('.top__slider-inner'),
        width = window.getComputedStyle(slidesWrapper).width;

    let slideIndex = 1;

    let offset = 0;

    slidesField.style.width = 100 * slides.length + '%';
    slidesField.style.display = 'flex';
    slidesField.style.transition = '1.5s all';

    slidesWrapper.style.overflow = 'hidden';

    slides.forEach(slide => {
        slide.style.width = width;
    });


    next.addEventListener('click', () => {
        if (offset === +width.slice(0, width.length - 2) * (slides.length - 1)){
            offset = 0;
        } else {
            offset += +width.slice(0, width.length - 2);
        }

        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    prev.addEventListener('click', () => {
        if (offset === 0){
            (offset = +width.slice(0, width.length - 2) * (slides.length - 1));
        } else {
            offset -= width.slice(0, width.length - 2);
        }

        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    setInterval(function(){
        next.click();
    }, 10000000);

});
