document.addEventListener('DOMContentLoaded', function() {

    // убираем плейсхолдер при фокусе jQuery

    $('input,textarea').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
            .attr('placeholder','');
    }).blur(function(){
        $(this).attr('placeholder',$(this).data('placeholder'));
    })



    //Слайдер на главной vanila JS

    const slides = document.querySelectorAll('.offer__slide'),
          prev = document.querySelector('.prev'),
          next = document.querySelector('.next'),
          slidesWrapper = document.querySelector('.top__slider-wrapper'),
          slidesField = document.querySelector('.top__slider-inner'),
          width = window.getComputedStyle(slidesWrapper).width;

    let slideIndex = 1;

    let offset = 0;

    slidesField.style.width = 100 * slides.length + '%';
    slidesField.style.display = 'flex';
    slidesField.style.transition = '1.5s all';

    slidesWrapper.style.overflow = 'hidden';

    slides.forEach(slide => {
       slide.style.width = width;
    });


    next.addEventListener('click', () => {
        if (offset === +width.slice(0, width.length - 2) * (slides.length - 1)){
            offset = 0;
        } else {
            offset += +width.slice(0, width.length - 2);
        }

        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    prev.addEventListener('click', () => {
        if (offset === 0){
            (offset = +width.slice(0, width.length - 2) * (slides.length - 1));
        } else {
            offset -= width.slice(0, width.length - 2);
        }

        slidesField.style.transform = `translateX(-${offset}px)`;
    });

    setInterval(function(){
        next.click();
    }, 10000000);


    $('.sub__menu').hover(
        function() {
            $('.sub').css('display', 'block');
        }, function() {
            $('.sub').css('display', 'none');
        }
    );

    $( ".sub.fade" ).hover(function() {
        $( this ).fadeOut( 100 );
        $( this ).fadeIn( 500 );
    });

    $('.mobile__menu').click(function() {
       $('.menu__mobile-container').addClass('open-mobile');
       $('body').addClass('open-modal');
    });

    $('.close__button-menu').click(function(){
        $('.menu__mobile-container').removeClass('open-mobile');
        $('body').removeClass('open-modal');
    });

    $('.open-modal').click(function(){
        $('.menu__mobile-container').removeClass('open-mobile');
        $('body').removeClass('open-modal');
    });

    $(document).mouseup(function (e) {
        let container = $('.open-mobile');
        if (container.has(e.target).length === 0){
            $('.menu__mobile-container').removeClass('open-mobile');
            $('body').removeClass('open-modal');
        }
    });









})



















