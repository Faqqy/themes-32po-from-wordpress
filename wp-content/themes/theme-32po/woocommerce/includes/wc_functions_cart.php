<?php
if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.0' );
}
add_action( 'woocommerce_after_main_content', 'theme_32po_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
<?php
if ( function_exists( 'theme_32po_woocommerce_header_cart' ) ) {
theme_32po_woocommerce_header_cart();
}
?>
 */

if ( ! function_exists( 'theme_32po_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function theme_32po_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		theme_32po_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'theme_32po_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'theme_32po_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function theme_32po_woocommerce_cart_link() {
		?>
            <div class="cart">
                <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'theme-32po' ); ?>">

                <button class="btn cart__button" type="button"><img src="/wp-content/themes/theme-32po/assets/images/cart2.png" alt="Корзина покупок" class="cart__icon"></button>
                <div class="count">
                        <span class="counts"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ); ?></span>
                </div>
                </a>
            </div>

		<?php
	}
}

if ( ! function_exists( 'theme_32po_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function theme_32po_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php theme_32po_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}
