<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package 32po
 */
$editphotobanner1 = carbon_get_theme_option('po_photo_banner1');
$editphotobanner2 = carbon_get_theme_option('po_photo_banner2');
$editphotobanner3 = carbon_get_theme_option('po_photo_banner3');

$editphototext1 = carbon_get_theme_option('po_photo_text_banner1');
$editphototext2 = carbon_get_theme_option('po_photo_text_banner2');
$editphototext3 = carbon_get_theme_option('po_photo_text_banner3');

$editpromtext = carbon_get_theme_option('po_text_prom');

get_header();
?>

    <div class="wrapper">
        <section class="top">
            <div class="top__slider-wrapper">
                <div class="next" style="display: none;"> > </div>
                <div class="prev" style="display: none;"> < </div>
                <div class="top__slider-inner">
                    <div class="offer__slide" draggable="true" style="background: #4B4B4B;"> <!--style="background-image: url(images/antallaktika-fonto.jpg);" -->
                        <div class="container info__banner d-flex align-items-center justify-content-start">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="text">
                                        <h2>
                                            <?php echo $editphototext1; ?>
                                        </h2>
                                    </div>
                                    <div class="button">
                                        <button class="button button__red">
                                            <span>Подробнее</span> <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow.svg" alt="Подробнее">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="offer__slide" draggable="true" style="background: #4B4B4B;"> <!--style="background-image: url(images/antallaktika-fonto.jpg);" -->
                        <div class="container info__banner ">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="text">
                                        <h2>
	                                        <?php echo $editphototext2; ?>
                                        </h2>
                                    </div>
                                    <div class="button">
                                        <button class="button button__red">
                                            <span>Подробнее</span> <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow.svg" alt="Подробнее">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="offer__slide" draggable="true" style="background: #4B4B4B;"> <!--style="background-image: url(images/antallaktika-fonto.jpg);" -->
                        <div class="container info__banner ">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="text">
                                        <h2>
	                                        <?php echo $editphototext2; ?>
                                        </h2>
                                    </div>
                                    <div class="button">
                                        <button class="button button__red">
                                            <span>Подробнее</span> <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow.svg" alt="Подробнее">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="middle">
            <div class="bg__section-middle"></div>
            <div class="info d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 d-flex flex-column align-items-center justify-content-center">
                            <h1>Промоборудование</h1>
                            <p>
                                <?php echo $editpromtext; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vantage d-flex align-items-center">
                <div class="container">
                    <h1>Наши преимущества</h1>
                    <div class="row">
                        <div class="col-md-12 vantage__col d-flex flex-row align-items-center justify-content-between">
                            <div class="vantage__items">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/proizvod.png" alt="">
                                <p>
                                    Собственное производство
                                </p>
                            </div>
                            <div class="vantage__items">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nadez.png" alt="">
                                <p>
                                    Надежность и ответсвтенность
                                </p>
                            </div>
                            <div class="vantage__items">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/prof.png" alt="">
                                <p>
                                    Проффесионализм коллектива
                                </p>
                            </div>
                            <div class="vantage__items">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/price.png" alt="">
                                <p>
                                    Оптимальные и доступные цены
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="produce d-flex align-items-center">
            <div class="container">
                <h1>Наша продукция</h1>
                <div class="produce__grid" >
                    <div class="produce__item-first">
                        <p>Электрооборудование</p>
                    </div>
                    <div class="produce__item-second">
                        <div class="about__info">
                            <p>Электрооборудование для  трамвайных и моторных путей</p>
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/Arrow.svg" alt="Далее">
                        </div>
                    </div>
                    <div class="produce__item-third">
                        <p>Электрооборудование</p>
                    </div>
                    <div class="produce__item-fourth">
                        <p>Электрооборудование</p>
                    </div>
                </div>

            </div>
        </section>

        <section class="feedback d-flex flex-column align-items-center">
            <div class="container feedback__container">
                <h1>Обратная связь</h1>
                <p>Вы можете задать нам вопрос заполнив форму ниже и мы свяжемся с вами в кратчайшие сроки!</p>
                <div class="row">
                    <div class="col-md-12">
<!--                        <div class="feedback__form">-->
<!--                            <form action="#" name="form__fb" class="form d-flex flex-column">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6 col-xs-12">-->
<!--                                        <input type="text" name="name" placeholder="Ваше имя...">-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6 col-xs-12">-->
<!--                                        <input type="text" name="phone" placeholder="Ваш телефон...">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-12">-->
<!--                                        <textarea name="comment" id="#" cols="30" rows="10" placeholder="Ваш коментарий..."></textarea>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-12 container__politics d-flex justify-content-between">-->
<!--                                        <div class="politics">-->
<!--                                            <p>Нажимая кнопку Отправить вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>-->
<!--                                        </div>-->
<!--                                        <div class="button__send-form">-->
<!--                                            <button class="button button__red">-->
<!--                                                <span>Отправить</span> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/Arrow.svg" alt="Подробнее">-->
<!--                                            </button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <?php echo do_shortcode( '[contact-form-7 id="645" title="Форма обратной связи на главной странице"]' );?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="geography">
            <div class="container container__geography">
                <h1>География деятельности</h1>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="container__map">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/map.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php
get_footer();
