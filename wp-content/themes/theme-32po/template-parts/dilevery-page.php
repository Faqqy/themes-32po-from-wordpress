<?php
/**
 * Template Name: Доставка
 * */

$diletexttop = carbon_get_the_post_meta('po_dilevery_text_top');
$diletextbottom = carbon_get_the_post_meta('po_dilevery_text_bottom');

?>

<?php get_header(); ?>

<section class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 d-flex flex-row">
				<?php woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<div class="delivery">
	<div class="container">
		<div class="row">
			<p>
				<?php echo $diletexttop; ?>
			</p>
			<div class="delivery__company d-flex flex-row align-items-center justify-content-between">
				<div class="delivery__company-item active">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/samovyvoz.png" alt="">
					</div>
				</div>
				<div class="delivery__company-item">
					<div class="image">
						<img style="margin-top: 30px;" src="<?php echo get_template_directory_uri();?>/assets/images/pek.png" alt="">
					</div>
				</div>
				<div class="delivery__company-item">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/delovielinee.png" alt="">
					</div>
				</div>
				<div class="delivery__company-item">
					<div class="image">
						<img style="width: 180px; margin-top: 34px;" src="<?php echo get_template_directory_uri();?>/assets/images/baikal.png" alt="">
					</div>
				</div>
			</div>

			<div class="delivery__conditions">
				<h1>Условия доставки</h1>
				<p>
					<?php echo $diletextbottom; ?>
				</p>
				<div class="delivery__text-calculator">
					<p>Калькулятор расчета стоимости доставки</p>
				</div>
			</div>

			КАЛЬКЛУЯТОР ВЫГРУЗИТЬ ПЛАГИНОМ ВОРДПРЕСС!
		</div>
	</div>
</div>

<?php get_footer(); ?>
