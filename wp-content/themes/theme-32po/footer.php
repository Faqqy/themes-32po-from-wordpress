<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 32po
 */

?>
<?php
$copyright = carbon_get_theme_option('po_footer_copyright');
$editphone = carbon_get_theme_option('po_header_telephone');
$editphone2 = carbon_get_theme_option('po_header_telephone2');
$editemail = carbon_get_theme_option('po_header_mail');
$editadress = carbon_get_theme_option('po_header_adress');

$socialvk = carbon_get_theme_option('po_header_socialvk');
$socialinst = carbon_get_theme_option('po_header_socialinst');
$socialfb = carbon_get_theme_option('po_header_socialfb');
?>

</div>

<footer class="footer d-flex align-items-center">

    <div class="menu__mobile-container">
        <div class="close__button-menu">
            <img src="<?php echo get_template_directory_uri();?>/assets/images/exit.svg" alt="#">
        </div>
        <ul class="menu__mobile list-group d-flex flex-column justify-content-end">
            <li class="menu__mobile-item active"><a href="<?php echo home_url('/');?>">Главная</a></li>
            <li class="menu__mobile-item sub__menu-mobile"><a href="/produktsiya/">Продукция</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/proizvodstvo/">Производство</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/uslugi/">Услуги</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/informatsiya/">Информация</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/novosti/">Новости</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/pomoshh/">Помощь</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/dostavka/">Доставка</a></li>
            <li class="menu__mobile-item list-group-item"><a href="/kontakty/">Контакты</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 d-flex flex-column justify-content-start">
                <div class="logo">
	                <?php $logo_id = carbon_get_theme_option('po_footer_logo');
	                $logo = $logo_id ? wp_get_attachment_image_src($logo_id, 'full') : '';
	                ?>
	                <?php $logo_id = carbon_get_theme_option('po_footer_logo');
	                $logo = $logo_id ? wp_get_attachment_image_src($logo_id, 'full') : '';
	                ?>
                    <a href="<?php echo home_url('/');?>"><img class="logo__header" src="<?php echo $logo[0];?>" alt="Промоборудование"></a>
                    <p class="text__logo d-flex align-items-center">
                        Более 100 лет успешной поставки
                        оборудования и запчастей
                    </p>
                </div>
            </div>

            <div class="col-md-3 col">
                <ul class="menu__footer list-group">
                    <li class="menu__item-footer list-group-item"><a href="#">Главная</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Продукция</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">На складе</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Производство</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Спецпредложения</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Неликвиды</a></li>
                </ul>
            </div>

            <div class="col-md-3 col">
                <ul class="menu__footer list-group">
                    <li class="menu__item-footer list-group-item"><a href="#">Услуги</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Документация</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Техническая</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Новости</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Доставка</a></li>
                    <li class="menu__item-footer list-group-item"><a href="#">Контакты</a></li>
                </ul>
            </div>

            <div class="col-md-3 col-xs-12">
                <div class="contacts d-flex justify-content-center">
                    <div class="contacts__footer">
                        <div class="contacts__phone contact__adress">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/phone-icon.svg" alt="Наш телефон">
                            <span><a href="tel:<?php echo $editphone;?>"><?php echo $editphone;?>,</a></span>
                            <span><a href="tel:<?php echo $editphone2;?>"><?php echo $editphone2;?></a></span>
                            <br>
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/mail.png" alt="Наша почта" style="width: 20px;">
                            <span><a href="mailto:<?php echo $editemail;?>"><?php echo $editemail;?></a></span>
                            <br>
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/adress-icon.svg" alt="Наш адрес">
                            <span><?php echo $editadress;?></span>
                        </div>
                        <div class="social social__footer">
                            <div class="social__icons">
                                <a class="social__icons-item" href="<?php echo $socialvk;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/vk-icons.svg" alt="Вконтакте"></a>
                                <a class="social__icons-item" href="<?php echo $socialinst;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/inst-icons.svg" alt="Инстаграмм"></a>
                                <a class="social__icons-item" href="<?php echo $socialfb;?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/fb-icons.svg" alt="Фейсбук"></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="col-12 d-flex justify-content-center">
            <div class="copyright-text">
						<span>
                            <?php echo $copyright;?>
						</span>
            </div>
        </div>
    </div>
</div>

</body>
</html>
